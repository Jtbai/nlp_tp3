from sklearn.linear_model import LogisticRegression, LogisticRegressionCV
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.svm import SVC
from sklearn.pipeline import Pipeline
from utils.mongo_manager import MongoManager
import xml.etree.ElementTree as ET
import xml.etree

xml_path = 'data/yahoo-qa/FullOct2007.xml.part2'

connection_detail = {
        'db_name': 'datasets',
        'collection': 'yahoo_answers'
    }

template = {
        'cat': None, 'subject': None, 'content': None, 'bestanswer': None
    }
mongo_manager = MongoManager(connection_detail, template)

class QuestionXmlParser:

    def __init__(self, xml_file):
        self.xml_file = xml_file
        self.opened_document = {}
        self.tag_to_extract = None

    def get_information(self,tags_to_extract):
        self.tag_to_extract = tags_to_extract
        for event, elem in ET.iterparse(xml_path, events=('start', 'end', 'start-ns', 'end-ns')):
            # print(event)
            self.__init_new_output_documnet(event, elem)
            self.__prepare_output_document(event, elem)
            if event == "end" and elem.tag == "document":
                yield self.opened_document

    def __init_new_output_documnet(self,event,elem):
        if event == "start" and elem.tag == "document":
            self.opened_document = {}

    def __prepare_output_document(self,event, elem):
        if event == "start" and elem.tag in self.tag_to_extract:
            self.opened_document[elem.tag] = elem.text
import json

def extract_question_and_push_to_mongo():

    docs = json.load(open('cats.json'))
    # docs = {}


    unsorted_docs = [(key, value) for key, value in docs.items()]
    sorted_docs = [x[0] for x in sorted(unsorted_docs, key=lambda x: -x[1])[:10]]

    cat_to_use = sorted_docs[:10]

    parser = QuestionXmlParser(xml_path)
    # information_to_extract = ['cat','subject','content','bestanswer']
    information_to_extract = ['cat','subject','content','bestanswer']

    try :
        for nb_doc, doc in enumerate(parser.get_information(information_to_extract)):
            # print(nb_doc)

            if nb_doc % 10000 == 0:
                print(nb_doc)

            try:
                if doc['cat'] in cat_to_use:
                    mongo_manager.insert(doc)
            except Exception as e:
                print(e)
                # docs.append(";".join([str(doc.get(x,"")).strip().replace("\n","").replace("<br />","") for x in information_to_extract]))
    except Exception as e:
        print(e)


import sys
documents_2 = mongo_manager.get_all_documents()
X = ["{} {} {}".format(x['content'], x['subject'], x['bestanswer']) for x in documents_2]

documents = mongo_manager.get_all_documents()
Y = [x['cat'] for x in documents]

mongo_manager.close()

from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=.20, random_state=1337)
from collections import Counter

print("{} vs {}".format(Counter(y_train),len(y_train)))
print("{} vs {}".format(Counter(y_test),len(y_test)))

vectorizer = CountVectorizer(max_features=50000)
transformer = TfidfTransformer()
classifier_bow = LogisticRegression(multi_class="multinomial",solver="newton-cg")
# classifier_bow = LogisticRegression()
# classifier_tf = SVC()


import time
start_time = time.time()

pipeline_bow = Pipeline([('vectorizer',vectorizer), ('classifier',classifier_bow)])
# pipeline_tfidf = Pipeline([('vectorizer',vectorizer), ('transformer',transformer), ('classifier',classifier_bow)])

pipeline_bow.fit(x_train,y_train)
print(accuracy_score(y_test, pipeline_bow.predict(x_test)))


print("--- %s seconds ---" % (time.time() - start_time))
